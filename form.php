<html>
<title>My Car Lookup</title>
<head>
    <link rel="stylesheet" type="text/css" href="style.php"/>
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script>
        function submit_soap(){
            let plate=$("#plate_no").val();
            let state=$("#state").val();
            $.post("searchCars.php",{plate:plate,state:state},
            function(data){
            $("#xml_response").html(data);
            });
        }  
    </script>
</head>
<body>
    <div class="myGarage">
        <div class="title">
            <h3>My Garage</h3>
            <h4>Add your vehicles here to shop products that fit.</h4>
        </div>
        <h3>Let's start by adding your vechicle</h3>
        <form id="myForm" method="post">
            <label for="plate_no"><strong>Step 1.</strong> Do you know the plate no.</label><br>
            <input name="plate_no" id="plate_no" type="text" class="input"/><br>
            <label for="state"><strong>Step 2.</strong> Enter your state.</label><br>
            <select id="state" name="state" placeholder="Select an option" class="input">
                <option value="" disabled selected>Select your state</option>
                <option value="nsw">New South Wales</option>
                <option value="qld">Queensland/option>
                <option value="sa">South Australia</option>
                <option value="tas">Tasmania</option>
                <option value="vic">Victoria</option>
                <option value="wa">Western Australia</option>
            </select><br>
            <input type="button" value="Find" onclick="submit_soap()" class="button"/>
        </form> 
    <br><br><br>
        <div id="xml_response" class=""></div><br>   
    </div>
</body>
</html>