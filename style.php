<?php
    header('content-type: text/css; charse:UTF-8');
?>

.myGarage {
    padding: 20px;
    margin: 100px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 500px
    
}

.myGarage:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.title{
    display:flex;
    background-color: 	#D3D3D3;
    padding: 10px;
    align-content: center;
}

.title h3{
    color: black;
    font-size: 25px;
    margin-right: 50px;
    width: 50%;
}

.title h4{
    color: grey;
    font-size: 20px;
    margin-left: 10px;
}

.input {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
}

.button{
    position: absolute;
    width: 13%;
    height: 35px;
    background-color:#0a0a23;
    color: yellow;
    border:none;
    border-radius:20px;
}

.button:hover{
    background-color:yellow;
    color: black;
}

.button:active{
    background-color: pink;
    color: black;
}

.result{
    border: 1px solid red;
    padding: 10px;
}

